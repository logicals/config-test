// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class AssertWithoutMessage
{
  @Test
  public void test()
  {
    boolean isThisWorking = true;
    boolean isThisFunctional = true;
    boolean isThisANiceSample = isThisWorking && isThisFunctional;

    assertTrue(isThisANiceSample);
  }
}
