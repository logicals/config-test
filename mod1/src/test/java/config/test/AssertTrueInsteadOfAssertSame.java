// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class AssertTrueInsteadOfAssertSame
{
  @Test
  public void test()
  {
    boolean requiredValue = true;
    boolean actualValue = true;

    assertTrue("The actual value does not match the required value.", actualValue == requiredValue);
  }
}
