// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

public class DroppedException
{
  public DroppedException()
  {
    new IllegalArgumentException();
  }
}