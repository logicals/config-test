// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

public final class SwitchWithoutDefault
{
  private static final int CRAZY_VALUE1 = 123;
  private static final int CRAZY_VALUE2 = 321;
  private static final int NUMBER_OF_THE_BEAST = 666;

  public void runSwitchWithoutDefault(int number)
  {
    switch (number)
    {
      case CRAZY_VALUE1:
        // do nothing
        break;

      case CRAZY_VALUE2:
        // still do nothing
        break;

      case NUMBER_OF_THE_BEAST:
        // run away weeping
        break;
    }
  }
}
