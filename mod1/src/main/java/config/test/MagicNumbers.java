// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

public class MagicNumbers
{
  public String acceptedNumbers()
  {
    int ints = -1 + 0 + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9;
    long longs = -1L + 0L + 1L + 2L + 3L + 4L + 5L + 6L + 7L + 8L + 9L;
    double doubles = 0.0D + 1.0D;
    float floats = 0.0F + 1.0F;

    return String.format("%d %d %f %f", ints, longs, doubles, floats);
  }

  public String magicNumbers()
  {
    int ints = -42 + -2 + 10 + 99 + 666;
    long longs = -42L + -2L + 10L + 99L + 666L;
    double doubles = -1D + 0.1D + 1.1D;
    float floats = -1F + 0.1F + 1.1F;

    return String.format("%d %d %f %f", ints, longs, doubles, floats);
  }
}
