// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

public class BadComparison
{
  private static final long NUMBER_TOO_BIG_FOR_INTEGER = 12345678901L;

  public boolean beBuggy()
  {
    int value = 3;
    return value == NUMBER_TOO_BIG_FOR_INTEGER;
  }
}