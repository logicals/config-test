// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

public class CatchedIllegalMonitorStateException
{
  public void beBuggy()
  {
    try
    {
      doSomething();
    }
    catch (IllegalMonitorStateException exception)
    {
      doSomething();
    }
  }

  public Object doSomething()
  {
    return null;
  }
}