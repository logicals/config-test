// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

public class HardcodedIP
{
  public String getLocalhostIP()
  {
    return "127.0.0.1";
  }
}