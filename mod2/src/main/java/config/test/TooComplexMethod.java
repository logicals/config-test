// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

public final class TooComplexMethod
{
  private static final int CRAZY_VALUE1 = 771823;
  private static final int CRAZY_VALUE2 = 123456;
  private static final int CRAZY_VALUE3 = 654321;
  private static final int CRAZY_VALUE4 = 643221;
  private static final int CRAZY_VALUE5 = 653321;
  private static final int RESULT_IF_NUMBER_IS_CRAZY_VALUE1 = 666;
  private static final int FIRST = 0;
  private static final int ANSWER_TO_LIFE_THE_UNIVERSE_AND_EVERYTHING = 42;

  public int doMagic(int someNumber, String someString, boolean isSomethigUp)
  {
    int result;

    if (someNumber == CRAZY_VALUE1)
    {
      result = RESULT_IF_NUMBER_IS_CRAZY_VALUE1;
    }
    else if (isSomethigUp)
    {
      switch (someString)
      {
        case "do-crazy-magic":
          result = "this is very crazy.".toCharArray()[FIRST];
          break;

        case "recursive-madness":
          result = doMagic(CRAZY_VALUE2, "WOOP WOOP WOOP", true);
          break;

        case "WOOP WOOP WOOP":
          result = doMagic(CRAZY_VALUE3, "AAARGH", false);
          break;

        case "derpington":
        default:
          if (someNumber == ANSWER_TO_LIFE_THE_UNIVERSE_AND_EVERYTHING)
          {
            result = CRAZY_VALUE4;
          }
          else
          {
            result = CRAZY_VALUE5;
          }
      }
    }
    else
    {
      result = ANSWER_TO_LIFE_THE_UNIVERSE_AND_EVERYTHING;
    }

    return result;
  }
}
