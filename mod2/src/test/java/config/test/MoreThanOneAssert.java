// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package config.test;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class MoreThanOneAssert
{
  @Test
  public void test()
  {
    boolean isThisWorking = true;
    boolean isThisFunctional = true;
    boolean isThisANiceSample = isThisWorking && isThisFunctional;

    assertTrue("This is not working.", isThisWorking);
    assertTrue("This is not functional.", isThisFunctional);
    assertTrue("This is not a nice sample.", isThisANiceSample);
  }
}
